#!/usr/bin/env bash

# set -x

source ci/utils.sh

umask 0022

function create_module_file {
    export software=$1
    export version=$2
    export conda_path=${CONDA_HOME}/bin/
    export env_path=${CONDA_HOME}/envs/${software}-${version}
    export env_content=$(run "ls ${env_path}")

    run "mkdir -p ${MODULEFILES_PATH}/${software}"

    if [ "$REMOTE_MODE" = true ]; then
      module_file="/tmp/modulefile"
    else
      module_file="${MODULEFILES_PATH}/${software}/${version}"
    fi

    j2 ci/conda/modulefile.j2 tools/${software}/${version}/meta.yml > ${module_file}
    cat ${module_file}

    if [ "$REMOTE_MODE" = true ]; then
      scp $module_file ${CLUSTER_USER}@${CLUSTER_HOST}:${MODULEFILES_PATH}/${software}/${version}
      rm /tmp/modulefile
    fi

}

CONDA_BIN=$CONDA_HOME/bin/conda
MAMBA_BIN=$CONDA_HOME/bin/mamba
DEFAULT_BIN=$CONDA_BIN
CONDA_VERSION='4.8.4'
MAMBA_VERSION='0.5.1'

run "${CONDA_BIN} --version 2>&1" | grep "${CONDA_VERSION}"
if [ $? -ne 0 ]
then
    echo -e "\033[1mAuto Update Conda to ${CONDA_VERSION} \033[0m \xF0\x9F\x9A\x80"
    #    run "${CONDA_BIN} update conda --yes"
    run "${CONDA_BIN} install conda=${CONDA_VERSION} --yes"
fi
run "${CONDA_BIN} --version"

run "${CONDA_BIN} list | grep  '^python '" | grep ' 3.'
if [ $? -ne 0 ]
then
    echo -e "\033[1mAuto Update Python to 3.6 \033[0m \xF0\x9F\x9A\x80"
    run "${CONDA_BIN} install python=3.6 -c conda-forge --yes"
fi

run "ls ${MAMBA_BIN}"
if [ $? -ne 0 ]
then
    echo -e "\033[1mInstall Mamba\033[0m \xF0\x9F\x9A\x80"
    run "${CONDA_BIN} install mamba=${MAMBA_VERSION} -c conda-forge --yes"
else
    run "${CONDA_BIN} list | grep  '^mamba '" | grep "${MAMBA_VERSION}"
    if [ $? -ne 0 ]
    then
        echo -e "\033[1mAuto Update Mamba to ${MAMBA_VERSION} \033[0m \xF0\x9F\x9A\x80"
        run "${CONDA_BIN} install mamba=${MAMBA_VERSION} -c conda-forge --yes"
    fi
fi
run "${MAMBA_BIN} --version"

tools_list=$1

exit_code=0

# creating envs
for software_line in $(cat $tools_list); do

    software=$(echo $software_line | cut -f1 -d/);
    version=$(echo $software_line | cut -f2 -d/);

    CONDA_BIN=$DEFAULT_BIN
    ### Mamba hook
    if [ -f tools/${software}/${version}/mamba.hook ]
    then
        CONDA_BIN=$MAMBA_BIN
    fi

    ### Remove hook
    if [ -f tools/${software}/${version}/remove.hook ]
    then
        echo -e "\033[1mRemove Hook Find for ${software} ${version}\033[0m \xF0\x9F\x9A\x80"
        run "${CONDA_BIN} env remove --name ${software}-${version} --yes" 2> stderr
    fi

    env_file="tools/${software}/${version}/env.yml"
    if [ "$REMOTE_MODE" = true ]; then
      env_file_path="/tmp/${CI_JOB_ID}-env_file.yml"
      scp ${env_file} ${CLUSTER_USER}@${CLUSTER_HOST}:$env_file_path
    else
      env_file_path=$env_file
    fi

    echo -e "\033[1mDeploying ${software} ${version}\033[0m \xF0\x9F\x9A\x80"

    echo "Check if conda environment exist for ${software}-${version}"
    run "ls -d $CONDA_HOME/envs/${software}-${version}" 2> stderr
    if [ $? -ne 0 ]
       then
           echo "Creating ${software}-${version} conda environment..."
           run "${CONDA_BIN} env create -f ${env_file_path}" 2> stderr
           if [ $? -eq 0 ]; then
               echo "Creating modulefile..."
               create_module_file $software $version 2> /dev/null
           else
               echo -e "\xE2\x9D\x8C Error while creating"
               cat stderr
               exit_code=1
           fi
    else
        echo "Environment already exists"
        echo "Updating existing environment..."
        # https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#updating-an-environment
        run "${CONDA_BIN} env update -f ${env_file_path} --prune -v" 2> stderr
        if [ $? -eq 0 ]; then
            echo "Updating modulefile..."
            create_module_file $software $version
        else
            echo -e "\xE2\x9D\x8C Error while updating"
            cat stderr
            exit_code=1
        fi

    fi

    if [ "$REMOTE_MODE" = true ]; then
      run "rm $env_file_path"
    fi
done

exit $exit_code
