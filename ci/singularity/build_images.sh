#!/usr/bin/env bash

df -h

source ci/utils.sh

function create_module_file {
    export software=$1
    export version=$2
    export wrappers_path=${SINGULARITY_REPOSITORY_PATH}/wrappers/${software}/${version}

    j2 ci/singularity/modulefile.j2 tools/${software}/${version}/meta.yml
}

tools_list=$1

if [ -z $tools_list ]; then
  echo "No tools list provided"
  exit 1
fi

# creating envs
for software_line in $(cat $tools_list); do
  software=$(echo $software_line | cut -f1 -d/);
  version=$(echo $software_line | cut -f2 -d/);

  image_path=${SINGULARITY_REPOSITORY_PATH}/images/${software}-${version}.sif
  definition_path=tools/${software}/${version}/image.def

  LOCAL_SRC_UTILS_CMD="python3 ci/singularity/singularity_local_src_utils.py --software ${software} --version ${version} --test "
  if [ $($LOCAL_SRC_UTILS_CMD is_local_src_needed) -eq 1 ]; then
    if [ $($LOCAL_SRC_UTILS_CMD is_infra_software_src_path) -eq 0 ]; then
      echo "ABORT: the env variable SOFTWARE_SRC_PATH isn't defined for this infrastucture"
      echo -e "Skipped \xf0\x9f\xa6\x98"
      continue
    fi
    mkdir -p /src
    sshfs $CLUSTER_USER@$CLUSTER_HOST:$SOFTWARE_SRC_PATH /src
    if [ $($LOCAL_SRC_UTILS_CMD is_local_src_exists) -eq 0 ]; then
      echo "ABORT: some local src files are missing on this infrastucture"
      echo -e "Skipped \xf0\x9f\xa6\x98"
      continue
    fi
  fi

  echo -e "\033[1mDeploying ${software} ${version}\033[0m \xF0\x9F\x9A\x80"

  echo "Building Singularity image..."
  singularity build --force $image_path $definition_path

  if [ $? -ne 0 ]; then
      echo "Build failed"
      df -h
      exit 1
  fi

  echo "Creating wrappers..."
  python3 ci/singularity/build_wrappers.py $software $version

  echo "Creating modulefile..."
  mkdir -p ${MODULEFILES_PATH}/${software}
  create_module_file $software $version > ${MODULEFILES_PATH}/${software}/${version}
done
