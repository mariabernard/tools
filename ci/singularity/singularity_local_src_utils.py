#!/usr/bin/env python3

import argparse
import os
import sys

from yaml import load, Loader

CWD = os.path.abspath(os.getcwd())

def get_meta_file(software, version):
    meta_path = os.path.join(CWD, 'tools', software, version, "meta.yml".format(software, version))
    return(load(open(meta_path, "r"), Loader=Loader))


def is_local_src_needed(software, version):
    """Check if local src files are needed"""
    meta = get_meta_file(software, version)
    if len(meta.get('singularity_local_src', [])) > 0:
        print(1)
    else:
        print(0)


def is_infra_software_src_path(software, version):
    if ('SOFTWARE_SRC_PATH' in os.environ):
        print(1)
    else:
        print(0)


def is_local_src_exists(software, version):
    """Check if local src files actually exists"""
    meta = get_meta_file(software, version)

    # Check the src availability in local within the infrastructure
    for singularity_local_src in meta.get('singularity_local_src', []):
        src_path = os.path.join("/src", software, version, singularity_local_src)
        if not os.path.exists(src_path):
            sys.stderr.write("ERROR: Missing src file:"+src_path+"\n")
            print(0)
            sys.exit(142)
    print(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--software', help="Software name")
    parser.add_argument('--version', help="Software version number")
    parser.add_argument('--test', choices=('is_infra_software_src_path', 'is_local_src_needed', 'is_local_src_exists'))
    args = parser.parse_args()
    eval(args.test)(args.software, args.version)



